//
//  MovieUpcomingNowPlayBase.swift
//  KitaMovie
//
//  Created by Ardy Stefanus Sunarno on 16/10/21.
//

import Foundation

struct MovieUpcomingNowPlayBase : Codable {

        let dates : MovieUpcomingNowPlayDate?
        let page : Int?
        let results : [MovieUpcomingNowPlayResult]?
        let totalPages : Int?
        let totalResults : Int?

        enum CodingKeys: String, CodingKey {
                case dates = "dates"
                case page = "page"
                case results = "results"
                case totalPages = "total_pages"
                case totalResults = "total_results"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                dates = try values.decodeIfPresent(MovieUpcomingNowPlayDate.self, forKey: .dates)
                page = try values.decodeIfPresent(Int.self, forKey: .page)
                results = try values.decodeIfPresent([MovieUpcomingNowPlayResult].self, forKey: .results)
                totalPages = try values.decodeIfPresent(Int.self, forKey: .totalPages)
                totalResults = try values.decodeIfPresent(Int.self, forKey: .totalResults)
        }

}
