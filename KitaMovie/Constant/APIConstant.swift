//
//  APIConstant.swift
//  KitaMovie
//
//  Created by Ardy Stefanus Sunarno on 16/10/21.
//

import Foundation

struct APIConstant {
    
    static let MOVIE_BASE_URL = "https://api.themoviedb.org/3"
    static let MOVIE_API_KEY = "c2675034df651f52d111557ea674d02c"
    static let MOVIE_IMAGE_URL_BACKDROP = "https://image.tmdb.org/t/p/w300"
    static let MOVIE_IMAGE_URL_POSTER = "https://image.tmdb.org/t/p/original"
    static let MOVIE_LIST = "/movie/"
    
}
