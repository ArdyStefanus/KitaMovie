//
//  MovieListTableViewCell.swift
//  KitaMovie
//
//  Created by Ardy Stefanus Sunarno on 16/10/21.
//

import UIKit

class MovieListTableViewCell: UITableViewCell {

    @IBOutlet weak var viewBackCell: UIView!
    @IBOutlet weak var imgViewMovie: UIImageView!
    @IBOutlet weak var labelTitleMovie: UILabel!
    @IBOutlet weak var labelReleaseDateMovie: UILabel!
    @IBOutlet weak var labelOverviewMovie: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        viewBackCell.layer.cornerRadius = 15
        viewBackCell.layer.masksToBounds = false
        viewBackCell.layer.shadowRadius = 4
        viewBackCell.layer.shadowOpacity = 1
        viewBackCell.layer.shadowColor = UIColor.gray.cgColor
        viewBackCell.layer.shadowOffset = CGSize(width: 0 , height:2)
        viewBackCell.backgroundColor = .white
        
        imgViewMovie.layer.borderWidth = 1.0
        imgViewMovie.layer.masksToBounds = false
        imgViewMovie.layer.borderColor = UIColor.white.cgColor
        imgViewMovie.layer.cornerRadius = imgViewMovie.layer.frame.size.width / 2
        imgViewMovie.clipsToBounds = true
        imgViewMovie.contentMode = .scaleAspectFill
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
