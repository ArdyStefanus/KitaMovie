//
//  ViewController.swift
//  KitaMovie
//
//  Created by Ardy Stefanus Sunarno on 16/10/21.
//

import UIKit

var dictFavoriteList: [[String:String]] = []

class MovieListVC: BaseVC {

    var isPopularTopRated = true
    var resultDatumUpcoming: MovieUpcomingNowPlayBase?
    var resultDatumPopular: MoviePopularTopRatedBase?
    
    var tableViewListMovie = UITableView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigation()
        setTableView()
        
        getPopularTopRatedMovies(category: "popular") //Default list movie
    }
    
    @objc func actionFilter(){
        let alertController = UIAlertController(title: "Category", message:
            nil, preferredStyle: .actionSheet)
        alertController.addAction(UIAlertAction(title: "Popular", style: .default, handler: { [self]
            action in
            isPopularTopRated = true
            getPopularTopRatedMovies(category: "popular")
        }))
        alertController.addAction(UIAlertAction(title: "Upcoming", style: .default, handler: { [self]
            action in
            isPopularTopRated = false
            getUpcomingPlayingMovies(category: "upcoming")
        }))
        alertController.addAction(UIAlertAction(title: "Top Rated", style: .default, handler: { [self]
            action in
            isPopularTopRated = true
            getPopularTopRatedMovies(category: "top_rated")
        }))
        alertController.addAction(UIAlertAction(title: "Now Playing", style: .default, handler: { [self]
            action in
            isPopularTopRated = false
            getUpcomingPlayingMovies(category: "now_playing")
        }))
        let okAction = UIAlertAction(title: "Close", style: .cancel, handler: nil)
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @objc func actionFavorite(){
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "MovieFavoriteVC") as? MovieFavoriteVC
        navigationItem.backButtonTitle = "Favorite Movie"
        self.navigationController?.pushViewController(vc!, animated: true)
    }
}

extension MovieListVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isPopularTopRated == true{
            return resultDatumPopular?.results?.count ?? 0
        }
        else{
            return resultDatumUpcoming?.results?.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MovieListTableViewCell", for: indexPath) as! MovieListTableViewCell
        
        if isPopularTopRated == true{
            let dict = resultDatumPopular?.results![indexPath.row]
            let urlImage = URL(string: "\(APIConstant.MOVIE_IMAGE_URL_BACKDROP)\(dict?.backdropPath ?? "")")
            cell.imgViewMovie.downloadImageUrl(from: urlImage!)
            cell.labelTitleMovie.text = dict?.title
            cell.labelReleaseDateMovie.text = dict?.releaseDate
            cell.labelOverviewMovie.text = dict?.overview
        }
        
        else{
            let dict = resultDatumUpcoming?.results![indexPath.row]
            let urlImage = URL(string: "\(APIConstant.MOVIE_IMAGE_URL_BACKDROP)\(dict?.backdropPath ?? "")")
            cell.imgViewMovie.downloadImageUrl(from: urlImage!)
            cell.labelTitleMovie.text = dict?.title
            cell.labelReleaseDateMovie.text = dict?.releaseDate
            cell.labelOverviewMovie.text = dict?.overview
        }
        
        cell.selectionStyle = MovieListTableViewCell.SelectionStyle.none
        cell.backgroundColor = UIColor.clear
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 240
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "MovieDetailVC") as? MovieDetailVC
        
        if isPopularTopRated == true{
            let dict = resultDatumPopular?.results![indexPath.row]
            vc?.urlImageDetail = "\(APIConstant.MOVIE_IMAGE_URL_BACKDROP)\(dict?.posterPath ?? "")"
            vc?.titleMovieDetail = (dict?.title)!
            vc?.releaseDateDetail = (dict?.releaseDate)!
            vc?.overviewDetail = (dict?.overview)!
        }
        
        else{
            let dict = resultDatumUpcoming?.results![indexPath.row]
            vc?.urlImageDetail = "\(APIConstant.MOVIE_IMAGE_URL_BACKDROP)\(dict?.posterPath ?? "")"
            vc?.titleMovieDetail = (dict?.title)!
            vc?.releaseDateDetail = (dict?.releaseDate)!
            vc?.overviewDetail = (dict?.overview)!
        }
        navigationItem.backButtonTitle = "Movie Detail"
        self.navigationController?.pushViewController(vc!, animated: true)
    }
}

extension MovieListVC{
    
    func getPopularTopRatedMovies(category: String){
        showSpinner(onView: self.view)
        let urlString = "\(APIConstant.MOVIE_BASE_URL)\(APIConstant.MOVIE_LIST)\(category)?api_key=\(APIConstant.MOVIE_API_KEY)"
        let url = URL(string: urlString)
        
        guard url != nil else{
            debugPrint("URL is nil")
            return
        }
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: url!) { [self] (data, response, error) in
            
            if error == nil && data != nil{
                let jsonDecoder = JSONDecoder()
                do {
                    let BaseData = try jsonDecoder.decode(MoviePopularTopRatedBase.self, from: data!)
                    resultDatumPopular = BaseData
                    removeSpinner()
                    DispatchQueue.main.async {
                        self.tableViewListMovie.reloadData()
                    }
                }
                
                catch {
                    print("Error parse JSON")
                }
            }
        }
        dataTask.resume()
    }

    func getUpcomingPlayingMovies(category: String){
        showSpinner(onView: self.view)
        let urlString = "\(APIConstant.MOVIE_BASE_URL)\(APIConstant.MOVIE_LIST)\(category)?api_key=\(APIConstant.MOVIE_API_KEY)"
        let url = URL(string: urlString)
        
        guard url != nil else{
            debugPrint("URL is nil")
            return
        }
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: url!) { [self] (data, response, error) in
            
            if error == nil && data != nil{
                let jsonDecoder = JSONDecoder()
                do {
                    let BaseData = try jsonDecoder.decode(MovieUpcomingNowPlayBase.self, from: data!)
                    resultDatumUpcoming = BaseData
                    removeSpinner()
                    
                    DispatchQueue.main.async {
                        self.tableViewListMovie.reloadData()
                    }
                }
                
                catch {
                    print("Error parse JSON")
                }
            }
        }
        dataTask.resume()
    }
    
    func setTableView(){
        tableViewListMovie.delegate = self
        tableViewListMovie.dataSource = self
        
        let nib = UINib.init(nibName: "MovieListTableViewCell", bundle: nil)
        tableViewListMovie.register(nib, forCellReuseIdentifier: "MovieListTableViewCell")
        tableViewListMovie.backgroundColor = .white
        tableViewListMovie.separatorStyle = .none
        tableViewListMovie.showsVerticalScrollIndicator = false
        
        tableViewListMovie.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(tableViewListMovie)
        
        NSLayoutConstraint.activate([
            tableViewListMovie.topAnchor.constraint(equalTo: view.topAnchor),
            tableViewListMovie.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableViewListMovie.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableViewListMovie.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
    
    func setNavigation(){
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.barTintColor = .navColor

        let customView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 220.0, height: 44.0))
        
        let label_nav = UILabel(frame: CGRect(x: 0, y: 0, width: 180.0, height: 44.0))
        label_nav.text = "Kita Movie"
        label_nav.textColor = UIColor.white
        label_nav.textAlignment = .left
        label_nav.font = .systemFont(ofSize: 20)
        customView.addSubview(label_nav)
        
        let leftButton = UIBarButtonItem(customView: customView)
        self.navigationItem.leftBarButtonItem = leftButton
        
        let buttonFilter = UIBarButtonItem(image: UIImage(named: "Icon Filter"), style: .plain, target: self, action: #selector(actionFilter))
        let buttonFavorite = UIBarButtonItem(image: UIImage(named: "heart_fill"), style: .plain, target: self, action: #selector(actionFavorite))
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationItem.rightBarButtonItems = [buttonFilter, buttonFavorite]
    }
}
