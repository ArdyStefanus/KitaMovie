//
//  MovieFavoriteVC.swift
//  KitaMovie
//
//  Created by Ardy Stefanus Sunarno on 16/10/21.
//

import UIKit

class MovieFavoriteVC: BaseVC {
    
    var tableViewFavoriteList = UITableView()
    
    let deviceWidth = UIScreen.main.bounds.width
    let deviceHeight = UIScreen.main.bounds.height
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if dictFavoriteList.count == 0{
            setNoDataLabel()
        }
    }
}

extension MovieFavoriteVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dictFavoriteList.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MovieListTableViewCell", for: indexPath) as! MovieListTableViewCell
        
        let dict = dictFavoriteList[indexPath.row]
        
        let urlImage = URL(string: dict["urlImage"]!)
        cell.imgViewMovie.downloadImageUrl(from: urlImage!)
        
        cell.labelTitleMovie.text = dict["title"]
        cell.labelReleaseDateMovie.text = dict["release"]
        cell.labelOverviewMovie.text = dict["overview"]
        
        cell.selectionStyle = MovieListTableViewCell.SelectionStyle.none
        cell.backgroundColor = UIColor.clear
        
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 240
    }
}

extension MovieFavoriteVC{
    
    func setTableView(){
        tableViewFavoriteList.delegate = self
        tableViewFavoriteList.dataSource = self
        
        let nib = UINib.init(nibName: "MovieListTableViewCell", bundle: nil)
        tableViewFavoriteList.register(nib, forCellReuseIdentifier: "MovieListTableViewCell")
        tableViewFavoriteList.backgroundColor = .white
        tableViewFavoriteList.separatorStyle = .none
        tableViewFavoriteList.showsVerticalScrollIndicator = false
        
        tableViewFavoriteList.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(tableViewFavoriteList)
        
        NSLayoutConstraint.activate([
            tableViewFavoriteList.topAnchor.constraint(equalTo: view.topAnchor),
            tableViewFavoriteList.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableViewFavoriteList.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableViewFavoriteList.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
    
    func setNoDataLabel(){
        let label_kosong = UILabel()
        label_kosong.frame = CGRect(x: (Int(deviceWidth) / 2) - 87, y: (Int(deviceHeight) / 2) - 83, width: 180, height: 44)
        label_kosong.text = "No Data"
        label_kosong.textColor = UIColor.black
        label_kosong.textAlignment = .center
        label_kosong.font = UIFont.systemFont(ofSize: 18, weight: .light)
        self.view.addSubview(label_kosong)
    }
}
    
