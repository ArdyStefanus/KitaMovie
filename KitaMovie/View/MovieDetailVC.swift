//
//  MovieDetailVC.swift
//  KitaMovie
//
//  Created by Ardy Stefanus Sunarno on 16/10/21.
//

import UIKit

class MovieDetailVC: BaseVC {
    
    var tableViewDetailMovie = UITableView()
    var urlImageDetail = ""
    var titleMovieDetail = ""
    var releaseDateDetail = ""
    var overviewDetail = ""
    
    var isTapFavorite: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTableView()
    }
}

extension MovieDetailVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MovieDetailTableViewCell", for: indexPath) as! MovieDetailTableViewCell
        
        let urlImage = URL(string: urlImageDetail)
        cell.imgViewMovie.downloadImageUrl(from: urlImage!)
        cell.labelTitleMovie.text = titleMovieDetail
        cell.labelReleaseDateMovie.text = releaseDateDetail
        cell.dataOverviewMovie.text = overviewDetail
        
        cell.selectionStyle = MovieDetailTableViewCell.SelectionStyle.none
        cell.backgroundColor = UIColor.clear
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 380
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! MovieDetailTableViewCell
        if isTapFavorite == false{
            isTapFavorite = true
            cell.buttonFavorite.setBackgroundImage(UIImage(named: "heart_fill"), for: .normal)
            
            dictFavoriteList.append(["title": titleMovieDetail, "release": releaseDateDetail, "overview": overviewDetail, "urlImage": urlImageDetail])
        }
        else{
            isTapFavorite = false
            cell.buttonFavorite.setBackgroundImage(UIImage(named: "heart"), for: .normal)
            dictFavoriteList.removeLast()
        }
    }
}

extension MovieDetailVC{
    
    func setTableView(){
        tableViewDetailMovie.delegate = self
        tableViewDetailMovie.dataSource = self
        
        let nib = UINib.init(nibName: "MovieDetailTableViewCell", bundle: nil)
        tableViewDetailMovie.register(nib, forCellReuseIdentifier: "MovieDetailTableViewCell")
        tableViewDetailMovie.backgroundColor = .white
        tableViewDetailMovie.separatorStyle = .none
        tableViewDetailMovie.showsVerticalScrollIndicator = false
        
        tableViewDetailMovie.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(tableViewDetailMovie)
        
        NSLayoutConstraint.activate([
            tableViewDetailMovie.topAnchor.constraint(equalTo: view.topAnchor),
            tableViewDetailMovie.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableViewDetailMovie.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableViewDetailMovie.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
}
