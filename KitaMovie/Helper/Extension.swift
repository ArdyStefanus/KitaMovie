//
//  Extension.swift
//  KitaMovie
//
//  Created by Ardy Stefanus Sunarno on 16/10/21.
//

import UIKit

extension UIColor{
    static let navColor = UIColor(red: 41.0/255.0, green: 169.0/255.0, blue: 225.0/255.0, alpha: 1.0)
}

extension UIImageView{
    
     func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()){
       URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
     }
    
     func downloadImageUrl(from url: URL) {
        getData(from: url) {
           data, response, error in
           guard let data = data, error == nil else {
              return
           }
           DispatchQueue.main.async() {
              self.image = UIImage(data: data) ?? UIImage(named: "No Image")
           }
        }
     }
    func ResizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        var widthRatio: CGFloat = 0.0
        var heightRatio: CGFloat = 0.0
     
        //MEMPERKECIL UKURAN GAMBAR
        if targetSize.width > 2000 || targetSize.height > 2000{

            widthRatio  = targetSize.width * 28 / 100
            heightRatio = targetSize.height * 28 / 100

        } else if targetSize.width > 1000 || targetSize.height > 1000{

            widthRatio  = targetSize.width * 50 / 100
            heightRatio = targetSize.height * 50 / 100

        } else if targetSize.width > 500 || targetSize.height > 500{

            widthRatio  = targetSize.width * 70 / 100
            heightRatio = targetSize.height * 70 / 100

        } else {

            widthRatio  = targetSize.width
            heightRatio = targetSize.height
        }
        
        var newSize: CGSize
        if(widthRatio > heightRatio) {
           newSize = CGSize(width: widthRatio, height: heightRatio)
        } else {
           newSize = CGSize(width: widthRatio,  height: heightRatio)
        }
        
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
         
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()

        return newImage
    }
 }
